#Thank you for reviewing my challenge.

This version of the challenge is written in SwiftUI.

Upon lunch, API requests are made for the school data and sat score data.
NOTE: The app is setup to delay the request for SAT data to show the functionality when there is a delay in the SAT scores. When the SAT scores arrive, the school list screen (or the detail screen if it is displayed), is updated with SAT information.

The program will alert the user about communication errors and allow a retry.

When the school data is received, it is displayed in descending score order.

The user has the option to display it in alphabetical order.

The app will display the school names in red if there is no associated SAT score. If there is a delay in the score data arriving, all the names will be in red. When they arrive, the school names will change to black.

* Scores 1300-1600 will be displayed in green.
* Scores 1000-1299 will be displayed in orange.
* Scores 1-999 will be displayed in red.
* Score 0 means not available.

For the school detail page, I included school name, location and contact, score information, and overview. In addition to the score breakdowns, I have added a average total score field.

I have included a few running tests and a number of test methods whith no implementation, just to show examples of things that I would test. There are many more tests that I would write to fully test this app.

Ronnie Katz


