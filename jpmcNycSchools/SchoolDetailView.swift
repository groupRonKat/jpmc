//
//  SchoolInfoView.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/25/21.
//

import SwiftUI

/**
 Diplays school detail information in a scrolling form
 */
struct SchoolDetailView: View {
    
    @EnvironmentObject var appData: AppSchoolData
    
    //@State
    var dbn: String
     
    var body: some View {
        
        let schoolDetail = appData.schoolList.getData(dbn: dbn)!
        
        Form(content: {
            
            Section(header: Text("Name")) {
                Text("\(schoolDetail.school_name)")
                    .lineLimit(nil)
//                HStack {
//                    Text("School ID")
//                    Spacer()
//                    Text("\(schoolDetail.dbn)")
//                }
            }

            Section(header: Text("SAT Scores")) {

                if let schoolScores = appData.scoreList.getData(dbn: dbn) {
                    //if let schoolScores = schoolScoreData {
                    HStack {
                        Label("Number Of Takers", systemImage: "person.3")
                        Spacer()
                        Text("\(schoolScores.num_of_sat_test_takers)")
                    }

                    HStack {
                        Label("Reading", systemImage: "book")
                        Spacer()
                        Text("\(schoolScores.sat_critical_reading_avg_score)")
                    }

                    HStack {
                        Label("Math", systemImage: "textformat.123")
                        Spacer()
                        Text("\(schoolScores.sat_math_avg_score)")
                    }

                    HStack {
                        Label("Writing", systemImage: "pencil")
                        Spacer()
                        Text("\(schoolScores.sat_writing_avg_score)")
                    }

                    HStack {
                        Label("Total", systemImage: "sum")
                        Spacer()
                        Text("\(schoolScores.totalScore)")
                    }
                }
                else {
                    Text("There is no SAT score data available for this school.").foregroundColor(.red)
                }
            }

            Section(header: Text("Location and Contact")) {
                Label("\(schoolDetail.borough!)", systemImage: "building.2")
                    .lineLimit(nil)
                Label("\(schoolDetail.location)", systemImage: "mappin.and.ellipse")
                    .lineLimit(nil)
                Label("\(schoolDetail.phone_number)", systemImage: "phone")
                    .lineLimit(nil)
                Link(destination: URL(string: "http://\(schoolDetail.website)")!, label: {
                    Label("\(schoolDetail.website)", systemImage: "globe")
                        .lineLimit(nil)
                })
            }
            
            Section(header: Text("Overview")) {
                Text("\(schoolDetail.overview_paragraph ?? "?")")
                    .lineLimit(nil)
                    .font(.body)
            }
            
        })
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("SAT SCORES")
    }
    
}

//struct SchoolInfoView_Previews: PreviewProvider {
//    static var previews: some View {
//        SchoolInfoView()
//    }
//}
