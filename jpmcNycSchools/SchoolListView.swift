//
//  ContentView.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/24/21.
//

import SwiftUI

/**
 Displays list of schools. If the SAT score is available, it will be displayed also.
 If no SAT data is available, the name is displayed in red text
 */
struct SchoolListView: View {

    // common data is passed down through the view hierarchy
    @EnvironmentObject var appData: AppSchoolData

    // controls whether to display an isLoading message
    @Binding
    var isLoading: DataFetchStatus

    // when this changes, the school list is reordered and redisplayed
    @Binding
    var sortBy: OrderBy.RawValue

    // controls whether to display isLoading SAT scores
    @Binding
    var didReceivSATData: Bool 

    @State
    var message = "School data is being loaded"
    
    var body: some View {
        
        NavigationView {
            
            VStack {
                Picker("", selection: $sortBy) {
                    Text("Alphabetic").tag(OrderBy.alpha.rawValue)
                    Text("TotalScore").tag(OrderBy.total.rawValue)
                }
                .pickerStyle(SegmentedPickerStyle())

                switch appData.fetchStatus {

                case .isLoading:
                    Text("Requesting Data From Server")
                    ProgressView()

                case .isSuccess:

                    // display waiting for SAT message
                    if !didReceivSATData {
                        HStack {
                            Text("Waiting for SAT data...").foregroundColor(.red)
                            ProgressView()

                        }
                    }

                    // display the list
                    List(appData.getList(b: didReceivSATData), id: \.dbn) { school in

                        NavigationLink(
                            destination: SchoolDetailView(dbn: school.dbn)
                        ) {
                            // if there is no SAT data, make the text red
                            VStack(alignment: .leading) {
                                Text(school.school_name)
                                if didReceivSATData {
                                    if school.avgTotalSATScore > 0 {
                                        Text("(SAT: \(school.avgTotalSATScore))").foregroundColor(
                                            getColor(score: school.avgTotalSATScore))
                                    }
                                    else {
                                        Text("No SAT score data.").foregroundColor(Color.red)
                                    }
                                }
//                                else {
//                                    Text("SAT data not received yet").foregroundColor(Color.orange)
//                                }
                            }
                        }
                    }

                // there was an (communication) error, try again
                case .isError:
                    Form {
                        VStack(spacing: 10) {
                            Text("There seems to be a communication problem. Please check your data connection and try again")
                            Button("Retry") {
                                appData.fetchData()
                            }
                            //.buttonStyle(ButtonStyle())
                            .padding(7)
                            .overlay(
                                RoundedRectangle(cornerRadius: 7)
                                    .stroke(Color.black, lineWidth: 1) )
                        }
                    }
                }

                Spacer()
            }
            .navigationTitle("NYC Schools")
            .navigationBarTitleDisplayMode(.inline)
        }
    }

    /**
     Color the score based on score range
     */
    func getColor(score: Int) -> Color {
        if score > 1300 {
            return .green
        }

        if score > 1100 {
            return .orange
        }

        return .red
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView(, nycSchools: <#Binding<[NYCSchool]>#>)
//    }
//}
