//
//  jpmcNycSchoolsApp.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/24/21.
//

import SwiftUI

@main
struct jpmcNycSchoolsApp: App {

    @State
    var appData = AppSchoolData()

    var body: some Scene {

        WindowGroup {
            SchoolListView(
                isLoading: $appData.fetchStatus,
                sortBy: $appData.sortBy,
                didReceivSATData: $appData.didReceiveSATScores)
                .environmentObject(appData)
        }
    }
}
