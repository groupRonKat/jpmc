//
//  NYCSchool.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/24/21.
//

import Foundation

// Sort order of school list
public enum OrderBy: Int {
    case alpha = 0
    case total
}

// status of (school data) fetch request
public enum DataFetchStatus {
    case isLoading
    case isError
    case isSuccess
}

// reporting erros back from requests
public enum RequestError: Error {
    case CommError
    case badURL
}

/**
 Data model for the app
 */
class AppSchoolData: ObservableObject {

    // data for all of the schools
    @Published
    var schoolList: SchoolList

    // SAt data for the schools (not all schools have SAT records)
    @Published
    var scoreList: ScoreList

    // indicates whether
    @Published
    var didReceiveSATScores = false

    // indicates that we are waiting for the data for the the schools (not the SAT data)
    @Published
    var fetchStatus:DataFetchStatus = .isLoading

    // sorting alphabetically or descening by SAT score
    @Published
    var sortBy: Int = OrderBy.total.rawValue //OrderBy.alpha.rawValue

    var didCalculateTotals = false

    /**
        Finds the SAT scores based on the DBN. Totals it, and stores with the school data.
        This can be done, on the fly, without storing the totals, but there is a slight delay.
        Storing the totals makes the display faster when switch between alpha and scores.
     */
    func getTotals() {

        // for each school
        guard schoolList.schools.count > 0 else {
            print("nothing to total")
            return
        }

        for school in schoolList.schools {

            let schoolInfo = scoreList.scores.first { aSchoolInfo in
                aSchoolInfo.dbn == school.dbn
            }

            if schoolInfo != nil {

                if let total = schoolInfo?.totalScore {
                    school.avgTotalSATScore = total
                }
            }
        }

        didCalculateTotals = true
    }

    /**
     Request school data and SAT data from the source
     */
    public func fetchData() {

        // display the "loading" message
        fetchStatus = .isLoading
        
        print("fetch school list")
        schoolList.fetch() { result in
            
            switch result {
            
             case .success(let count):
                print("schools received \(String(describing: count)) items")

                // start with alphabetic sorting
                self.schoolList.sortByAlpha()

//                if !self.didCalculateTotals {
                    self.getTotals()
//                }

                DispatchQueue.main.async {
                    self.fetchStatus = .isSuccess
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    self.fetchStatus = .isError
                }
                print(error.localizedDescription)
            }
        }

        /**
         Fetch the score data from the server

         NOTE: For testing, putting a delay on this will show that the list screen will be automatically updated (with colors) when the score data has arrived
         */
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            print("fetch score list")

            self.scoreList.fetch() { result in
                
                switch result {
                
                case .success(let count):
                    print("scores received \(String(describing: count)) items")

                    self.getTotals()

                    DispatchQueue.main.async {
                        // update the list screen with colors showing sat score availability
                        self.didReceiveSATScores = true
                    }

                    DispatchQueue.main.async {
                        // update the list screen with colors showing sat score availability
                        self.objectWillChange.send()
                    }
                    
                case .failure(let error):
                    print("error with scores data: \(error.localizedDescription)")
                }
                
            }
        }
    }

    func getList(b: Bool) -> [SchoolData] {

        //print("*** getlist")
        if sortBy == OrderBy.total.rawValue {
            schoolList.sortByScoreTotal(scoreList: scoreList)
            return schoolList.schools
        }
        else {
            schoolList.sortByAlpha()
            return schoolList.schools
        }
    }

    init() {
        
        schoolList = SchoolList()
        
        scoreList = ScoreList()
        
        fetchData()
    }
}
