//
//  School.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/25/21.
//

import Foundation

/**
 Represents school data. Retrieved from server in JSON format and decoded
 */
class SchoolData: Decodable {
    // id
    var dbn: String = ""
    var school_name: String  = ""
    var borough: String? = "" // some do not have this field
    var boro: String = ""
    var overview_paragraph: String = ""
    var location: String = ""
    var phone_number: String = ""
    var website: String = ""

    // prevents decoding of avgTotalSATScore which is not in the json data
    private enum CodingKeys: String, CodingKey {
        case dbn, school_name, borough, boro, overview_paragraph, location, phone_number, website
    }

    var avgTotalSATScore = 0    
}

