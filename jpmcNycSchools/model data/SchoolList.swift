//
//  SchoolList.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/25/21.
//

import Foundation

/**
 For school data records
 */
class SchoolList: ObservableObject {

    @Published
    var schools: [SchoolData] = []

    /**
     Sort the schools alphabetically
     */
    public func sortByAlpha() {

        schools.sort(by: { lhs, rhs in
            return lhs.school_name < rhs.school_name
        })
    }

    /*
     Sort by previously calculated totals
     */
    public func sortByScoreTotal(scoreList: ScoreList) {

        // descending order by SAT score
        schools.sort(by: { lhs, rhs in
            return lhs.avgTotalSATScore > rhs.avgTotalSATScore
        })
    }

    /**
     Fetch school data asynchronously
     */
    func fetch(whenComplete: @escaping (_ result: Result<SchoolList?, Error>) -> Void) -> Void {

        let url = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"

        guard let url = URL(string: url) else {
            print("Invalid URL")
            return
        }

        let request = URLRequest(url: url)

        URLSession.shared.dataTask(with: request) { data, response, error in

            if let data = try? Data(contentsOf: url),
               let records = try? JSONDecoder().decode([SchoolData].self, from: data) {

                print("there are ", records.count, "NYC schools")
                self.schools = records

                whenComplete(.success(self))
            }
            else {
                whenComplete(.failure(RequestError.CommError))
            }
        }
        .resume()

        print("RETURNING FROM fetch call")
    }

    // used for reading json from file. For testing purposes
    func read() {

        guard let url = Bundle.main.url(forResource: "NYCSchools", withExtension: "json") else {
            return
        }

        if let data = try? Data(contentsOf: url),
            let schools = try? JSONDecoder().decode([SchoolData].self, from: data) {
            self.schools = schools
        }
    }

    /**
     return the schoolData for the given dbn id
     */
    func getData(dbn: String) -> SchoolData? {

        let item = schools.first(where: { schoolData in
            schoolData.dbn == dbn
        })

        return item
    }
}
