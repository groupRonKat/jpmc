//
//  SchoolInfo.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/25/21.
//

import Foundation
import SwiftUI

/**
 Represents school SAT data. Retrieved from server in JSON format and decoded
 */
class SchoolScoreData: Decodable {
    // id
    var dbn: String = ""
    var school_name: String  = ""
    var num_of_sat_test_takers: String = ""
    var sat_critical_reading_avg_score: String = ""
    var sat_math_avg_score: String = ""
    var sat_writing_avg_score: String = ""

    var totalScore: Int {
        return
            (Int(sat_critical_reading_avg_score) ?? 0) 
            + (Int(sat_math_avg_score) ?? 0)
            + (Int(sat_writing_avg_score) ?? 0)
    }
}

