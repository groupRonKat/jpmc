//
//  ScoreList.swift
//  jpmcNycSchools
//
//  Created by Ronnie Katz on 5/25/21.
//

import Foundation

/**
 For school SAT data records
 */
class ScoreList: ObservableObject {

    @Published
    var scores: [SchoolScoreData] = []

    /**
     Fetch school SAT data asynchronously
     */
    func fetch(whenComplete: @escaping (_ result: Result<ScoreList?, Error>) -> Void) -> Void {

        let url = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

        guard let url = URL(string: url) else {
            print("Invalid URL")
            whenComplete(.failure(RequestError.badURL))
            return
        }

        let request = URLRequest(url: url)

        URLSession.shared.dataTask(with: request) { data, response, error in

            if let data = try? Data(contentsOf: url),
               let records = try? JSONDecoder().decode([SchoolScoreData].self, from: data) {

                print("there are ", records.count, "score records")
                self.scores = records

                whenComplete(.success(self))
            }
        }
        .resume()

        print("RETURNING FROM fetch scores call")
    }

    // used for reading from json file. For test purposes
    func read() {

        guard let url = Bundle.main.url(forResource: "satData", withExtension: "json") else {
            return
        }

        if let data = try? Data(contentsOf: url),

           let scores = try? JSONDecoder().decode([SchoolScoreData].self, from: data) {

            self.scores = scores
        }
    }

    /**
     return the scoreData for the given dbn id
     */
    func getData(dbn: String) -> SchoolScoreData? {

        let sd = scores.first(where: { scoreData in
            scoreData.dbn == dbn
        })

        return sd
    }
}
