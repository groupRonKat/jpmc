//
//  jpmcNycSchoolsTests.swift
//  jpmcNycSchoolsTests
//
//  Created by Ronnie Katz on 5/25/21.
//

import XCTest

@testable
import jpmcNycSchools

class jpmcNycSchoolsTests: XCTestCase {

    override func setUpWithError() throws {

    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_school_list_initialization_using_file() throws {

        let schoolList = SchoolList()
        schoolList.read()
        XCTAssert(schoolList.schools.count == 440)
    }

    func test_score_list_initialization_using_file() throws {

        let scoreList = ScoreList()
        scoreList.read()
        XCTAssert(scoreList.scores.count == 478)
    }


    func test_school_list_sort_alphaetically_using_file() throws {

        let schoolList = SchoolList()
        schoolList.read()

        schoolList.sortByAlpha()

        XCTAssert(schoolList.schools.first?.dbn == "06M540")
        XCTAssert(schoolList.schools.last?.dbn == "02M047")
    }

    func test_school_list_sort_byScore_using_file() throws {
        // this sort uses the school list and score list to get results
        // good test
    }



}
