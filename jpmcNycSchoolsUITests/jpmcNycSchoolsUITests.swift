//
//  jpmcNycSchoolsUITests.swift
//  jpmcNycSchoolsUITests
//
//  Created by Ronnie Katz on 5/31/21.
//

import XCTest

class jpmcNycSchoolsUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_that_loading_is_displayed() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // ...

    }

    func test_that_loading_sat_scores_is_displayed_when_fetched_with_delay() throws {
        XCTSkip()    }


    func test_that_loading_disappears_when_data_is_received() throws {
        XCTSkip()
    }

    func test_that_list_is_displayed_sorted_by_total_when_data_is_received() throws {
        XCTSkip()    }

    func test_that_detail_is_displayed_when_item_selected() throws {
        XCTSkip()    }

    func test_that_sat_score_message_displayed_when_no_sat_data() throws {
        XCTSkip()    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
